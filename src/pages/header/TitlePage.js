import React from 'react';
import logo from './logo_transparent.png';

import './heading.css';

const TitlePage = () => {
    return (
        <div className="heading">
            <img src={logo} alt="logo"></img>
            <p>Web is under construction...</p>
        </div>
    );
}

export default TitlePage;