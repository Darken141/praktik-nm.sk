import React from 'react';
import TitlePage from './TitlePage';


const Header = () =>{
    return (
    <header>
        <TitlePage />
    </header>
    );
}


export default Header;
