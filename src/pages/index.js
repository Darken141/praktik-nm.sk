import React from "react"
import './App.css';
import Header from './header/Header';
import SEO from "../components/seo"

const IndexPage = () => (
  <div className="App">
    <SEO title="Predajňa PRAKTIK" />
    <Header />
  </div>
)

export default IndexPage
